module gitlab.com/nvidia/cloud-native/go-nvlib

go 1.20

require (
	github.com/NVIDIA/go-nvml v0.12.0-1
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
